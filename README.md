# Distant Visualization

Scripts and other documents relating to distant visualization work being done in the College of Arts and Letters at MSU

These scripts are designed to be run on *nix systems for pulling frames out of video files and creating csv files based off a set of image files in a directory. 

The scripts prepare the files for basic use with the ImagePlot plugin for ImageJ

Note: if you are using a Mac and are trying to run some of these files locally you'll need to ensure you are able to execute xargs from the mac.
To do this install homebrew and then do "brew install findutils" Once this is installed you'll need to change the files so they read gxargs instead of just xargs. On linux the xargs version of the file works fine.