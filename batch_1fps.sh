#!/bin/bash
# this script generates a set of images from any video file in the directory it sits at 1fps
# Script written by College of Arts and Letters at Michigan State University http://edtech.cal.msu.edu/

find . -maxdepth 1 -name \*.mp4 -print0 | xargs -0 -i sh -c 'mkdir "images/{}"; nice -n 10 ffmpeg -i "{}" -threads 0 -r 1 "images/{}/image-%d.jpg";'
