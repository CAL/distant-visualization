#!/bin/bash
# this script generates a csv file for with a filename, increasing number from 1, and random number between 1 and 50
# Script written by College of Arts and Letters at Michigan State University http://edtech.cal.msu.edu/

echo > output.csv
count=1
YSCALE=100
find * -maxdepth 0 -name "*.jpg" -print0 | \
    (while read -d $'\0' i; do echo \"$i\", \"$(echo $i | sed -e s/[^0-9]//g)\", \"$(($RANDOM%$YSCALE))\" >> output.csv; count=$((count+1));  done)
